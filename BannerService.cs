﻿using SP.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.SharePoint;
//using log4net;
using System.ServiceModel.Channels;
using System.Linq.Expressions;
using CamlexNET;

namespace SP.ESPORTAL.CIS.DATA
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BannerService
    {

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        public Message JTable(JTableModel para)
        {
            try
            {
                var urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"];
                var da = new BannerDA(urlList);
                var total = 0;
                var spquery = new SPQuery();
                var queryOrder = para.CamlQueryOrderBy;
                var keyword = para.search.value ?? "";
                var querySearch = da.QueryStringDynamicSearch(new List<string> { "Ord", "Title" }, keyword);
                var location = para.Dynamic["Location"].ToString();
                var title = para.Dynamic["title"].ToString();
                var expressions = new List<Expression<Func<SPListItem, bool>>>();
                if (!string.IsNullOrEmpty(location))
                    expressions.Add(x => ((string)x["Location"]).Contains(location));
                if (!string.IsNullOrEmpty(title))
                    expressions.Add(x => ((string)x["Title"]).Contains(title));
                var caml = Camlex.Query().WhereAll(expressions);
                spquery.Query = caml + queryOrder + querySearch;
                var items = da.GetItemCollectionPaging<BannerListView>(spquery, para.length, para.CurrentPage, ref total).ToList();
                var result = new JTableData<BannerListView> { data = items, draw = para.draw, recordsTotal = total, recordsFiltered = total };
                return result.ToJsonStream();
            }
            catch (Exception ex)
            {
                return new Paging<BannerListView>().ToJsonStream(); ;
            }
        }

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "getitem/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Banner GetItem(string id)
        {
            try
            {
                string urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"];
                var da = new BannerDA(urlList);
                return da.GetItemByID<Banner>(int.Parse(id));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        public JMessage Insert(Banner item)
        {           
            try
            {       
                string urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"];
                var da = new BannerDA(urlList);
                var msg = da.Add<Banner>(item);
                 
                return msg;
            }
            catch (Exception ex)
            { 
                var msg = new JMessage() { Error = true, Title = "Error. Insert Item." + ex.Message };
                return msg;

            }
        }
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        public JMessage Update(Banner item)
        {
            try
            {

                string urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"]; 
                var da = new BannerDA(urlList);
                var msg = da.Edit<Banner>(item);
                 
                return msg;
            }
            catch (Exception ex)
            {
                var msg = new JMessage() { Error = true, Title = "Error. Update Item." };
                return msg;

            }
        }
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        public JMessage Delete(List<int> items)
        {
            try
            {
                 
                string urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"];
                var da = new BannerDA(urlList); 
                var msg = da.Delete(items);
                return msg;
            }
            catch (Exception ex)
            {
                var msg = new JMessage() { Error = true, Title = "Error. Delete Item." };
                return msg;

            }
        }
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "getchoice/{field}", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        public List<string> GetChoice(string field)
        {

            try
            {
                var urlList = WebOperationContext.Current?.IncomingRequest.Headers["UrlListProcess"];
                var da = new BannerDA(urlList);
                return da.GetChoiceFieldValues(field);
            }
            catch (Exception)
            { return new List<string>(); }
        }

    }
} 